﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Fizzbuzz.Models;
using FizzBuzzRules.Interface;
using FizzBuzzRules.Rules; 
using PagedList;

namespace Fizzbuzz.Controllers
{
    public class FizzbuzzController : Controller
    {
        private readonly IFizzBuzzRepositary fizzBuzzRepositary;
        private const int pageSize = 20;

        public FizzbuzzController(IFizzBuzzRepositary fizzBuzzRepositary)
        {
            this.fizzBuzzRepositary = fizzBuzzRepositary;
        }

        [HttpGet]
        public ActionResult Index(string inputNumber, int? page)
        {
            var model = new FizzbuzzModel();
             
            if (!string.IsNullOrEmpty(inputNumber))
            {
                int.TryParse(inputNumber, out int number);
                model = this.GetFizzBuzzList(number, page);
                return View("Index", model);
            }

            return View("Index", model);

        }

        [HttpPost]
        public ActionResult Index(FizzbuzzModel fizzbuzz, int? page)
        {
            var model = new FizzbuzzModel();

            if (ModelState.IsValid)
            {
                int.TryParse(fizzbuzz.InputNumber, out int number);
                model = this.GetFizzBuzzList(number, page);

                return View("Index", model);
            }

            return View("Index", model);
        }

        private FizzbuzzModel GetFizzBuzzList(int userInput, int? page)
        {
            var result = this.fizzBuzzRepositary.GetResults(userInput);

            var pageNumber = page.HasValue ? page.Value : 1;

            var model = new FizzbuzzModel
            {
                InputNumber = userInput.ToString(),
                numbers = result.ToPagedList(pageNumber, pageSize)
            };

            return model;
        }
    }
}