﻿using System.ComponentModel.DataAnnotations;
using PagedList;

namespace Fizzbuzz.Models
{
    public class FizzbuzzModel
    {
        [Required(ErrorMessage = "Enter a number")]
        [Range(1, 1000, ErrorMessage = "Enter a number between 1 to 1000")]

        public string InputNumber { get; set; }

        public IPagedList<string> numbers { get; set; }

    }
}